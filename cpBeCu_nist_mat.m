function cpBeCu = cpBeCu_nist_mat(T)
    % Specific heat of BeCu C17510 only, where 83 K < T < 345 K.
    % Source: N. J. Simon, �. S. Drexler, and R. P. Reed, "Properties of Copper and Copper Alloys at Cryogenic Temperatures", NIST Monograph 177, 1992
    % https://nvlpubs.nist.gov/nistpubs/Legacy/MONO/nistmonograph177.pdf
    % 
    % Function by E. Ravaioli, TE-MPE-PE, CERN, Geneva, CH
    % August 2021
    
    % BeCu density: 8.25-8.36 g/cm^3
    density_BeCu = 8.3e3; % kg/m^3
    
    idxT1 = T<=10;
    idxT2 = (T> 10) & (T<=297);
    idxT3 = (T>297) & (T<=345);
    idxT4 = T>345;
    
    
    cpBeCu(idxT1) = 10.5737494;
    cpBeCu(idxT2) = -30.35 +4.2376*T(idxT2) -0.0147*T(idxT2).^2 +1.77285978322629E-05*T(idxT2).^3;
    cpBeCu(idxT3) = 346.5 +0.1667*T(idxT3);
    cpBeCu(idxT4) = 404.01149850;

    cpBeCu = cpBeCu *density_BeCu;
end