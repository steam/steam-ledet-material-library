function kBeCu = kBeCu_WiedemannFranz(T, f_scaling)
    % Thermal conductivity of C17200 Beryllium Copper in temperature range (4-300 K).
    % The function calculates the thermal conductivity by means of the Wiedemann-Franz
    % relation, and makes use of the electrical resistivity from the reference below.
    % 
    % Electrical Resistivity Dependence of C17200 Beryllium Copper on Test Temperature (4-300 K).
    % Source: N. J. Simon, �. S. Drexler, and R. P. Reed, "Properties of Copper and Copper Alloys at Cryogenic Temperatures", NIST Monograph 177, 1992
    % https://nvlpubs.nist.gov/nistpubs/Legacy/MONO/nistmonograph177.pdf
    % 
    % Function by E. Ravaioli, TE-MPE-PE, CERN, Geneva, CH
    % August 2021
    
    % Scaling factor (optional)
    if nargin<2
        f_scaling=1;
    end

    L0=2.45E-8; % [W*Ohm/K^2]
    rhoBeCu = rhoBeCu_nist_mat(T, f_scaling);
    kBeCu=L0*T./rhoBeCu;
