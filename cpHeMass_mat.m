function cpHe = cpHeMass_mat(T)
% % % Temperature range: 1-1000 K
% % % The formula gives the thermal capacitance in [J/K/kg]

cpHe=zeros(size(T));

idxT1=find(T<=2.17);
idxT2=find(T>2.17&T<=2.5);
idxT3=find(T>2.5&T<=4.3);
idxT4=find(T>4.3&T<=15);
idxT5=find(T>15);

cpHe(idxT1) = 2.12 +.000678*T(idxT1).^12.159;
cpHe(idxT2) = -274.45*T(idxT2).^3 +1961.6*T(idxT2).^2 -4673.2*T(idxT2) +3712.9;
cpHe(idxT3) = .9163*T(idxT3).^2 -4.484*T(idxT3) +7.68;
cpHe(idxT4) = 5.2 +1489.4*T(idxT4).^(-4.06);
cpHe(idxT5) = 5.2;

cpHe=cpHe*10^3; % [J/K/kg]

end