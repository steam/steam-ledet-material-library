function cpG10 = cpG10_nist(T)
   density=1900;
   a0=-2.4083;
   a1=7.6006;   
   a2=-8.2982;
   a3=7.3301;
   a4=-4.2386;
   a5=1.4294;
   a6=-0.24396;
   a7=0.015236;
   logT=log10(T);
   p=10^(a7*((logT)^7)+a6*((logT)^6)+a5*((logT)^5)+a4*((logT)^4)+a3*((logT)^3)+a2*((logT)^2)+a1*((logT))+a0);   
   cpG10=density*p;
end