function [Jc_T_B] = Jc_Nb3Sn_Summer(T, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn)
% Critical current density in a Nb3Sn strand, Summer fit
% Function by E. Ravaioli
% (C) Emmanuele Ravaioli
% LBNL, Berkeley, CA

% % % Check all inputs are scalars or vectors
if ( isvector(T) && isvector(B) && isvector(Jc_Nb3Sn0) && isvector(Tc0_Nb3Sn) && isvector(Bc20_Nb3Sn) )~=1
    error('All inputs must be scalars or vectors with the same number of elements.')
end

nElements=max([ length(T(:)), length(B(:)), length(Jc_Nb3Sn0(:)), length(Tc0_Nb3Sn(:)), length(Bc20_Nb3Sn(:)) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (length(T(:))>1 && length(T(:))~=nElements) || (length(B(:))>1 && length(B(:))~=nElements) ||...
        (length(Jc_Nb3Sn0(:))>1 && length(Jc_Nb3Sn0(:))~=nElements) ||...
        (length(Tc0_Nb3Sn(:))>1 && length(Tc0_Nb3Sn(:))~=nElements) ||...
        (length(Bc20_Nb3Sn(:))>1 && length(Bc20_Nb3Sn(:))~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end


% % % Modify the input magnetic field
B(B<0)=-B(B<0); % direction of the magnetic field is unimportant
B(abs(B)<.001)=.001; % very small magnetic field causes numerical problems
T(T==0)=.001; % temperature equal to zero causes numerical problems

% % % Critical Magnetic Field
f_T_T0=T./Tc0_Nb3Sn;
f_T_T0(f_T_T0>1)=1; % avoid values higher than 1
Bc2=Bc20_Nb3Sn.*(1-f_T_T0.^2).*(1-0.31*f_T_T0.^2.*(1-1.77*log(f_T_T0)));
f_B_Bc2=B./Bc2;
f_B_Bc2(f_B_Bc2>1)=1; % avoid values higher than 1

% % % Critical Current Density
Jc_T_B = Jc_Nb3Sn0./sqrt(B).*(1-f_B_Bc2).^2.*(1-f_T_T0.^2).^2;

end