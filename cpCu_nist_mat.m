function cpCu = cpCu_nist_mat(T)
% Temperature range: 4-300 K
% Source: NIST, https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm
% Note: Even if the NIST fit is valida between 4-300 K, it is a better approximation 
% to extend it to 1-400 K, and possibly even 1-1000 K.

   density = 8960; % [kg/m^3] No value in NIST database, so most trustworthy value
   
   cpCu_perMass=zeros(size(T));

%    T(T<4)=4; % Note by Emmanuele Ravaioli: Even if the NIST fit is valida between 4-300
%    K, it is a much better approximation to extend it to 1-300 K (corrected on 2021/08/23)
    T(T<1)=1;
   
   idxT1=find(T<300);
   idxT2=find(T>=300);

   dc_a = -1.91844;
   dc_b = -0.15973;
   dc_c = 8.61013;
   dc_d =-18.996;
   dc_e = 21.9661;
   dc_f =-12.7328;
   dc_g =  3.54322;
   dc_h = -0.3797;
   
   logT1=log10(T(idxT1));
   tempVar = dc_a + dc_b * (logT1).^1 + dc_c * (logT1).^2  + dc_d * (logT1).^3 + dc_e * (logT1).^4 + dc_f * (logT1).^5 + dc_g * (logT1).^6 + dc_h * (logT1).^7;
   cpCu_perMass(idxT1)=10.^tempVar;
   
   cpCu_perMass(idxT2)=361.5+0.093*T(idxT2);
   
   cpCu= density*cpCu_perMass;
end