function kCu = kCu_WiedemannFranz(T, B, RRR, fScaling_MR)
% % % Wiedemann-Franz formula:

    if nargin<4
        fScaling_MR=1; % default, no change of the magneto-ressitivity
    end

    L0=2.45E-8; % [W*Ohm/K^2]

    rhoCu = rhoCu_nist(T,B,RRR,fScaling_MR);
    
    kCu=L0*T./rhoCu;
end