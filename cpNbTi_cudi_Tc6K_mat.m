function cpNbTi = cpNbTi_cudi_Tc6K_mat(T)
% Temperature range: 1-500 K
% The non-linear jump in the heat capacity which occurs at the critical is
% here assumed to always occur at T=6 K
Tc=6;

cpNbTi=zeros(size(T));

idxT1=find(T<=Tc);
idxT2=find(T>Tc&T<=20);
idxT3=find(T>20&T<=50);
idxT4=find(T>50&T<=175);
idxT5=find(T>175&T<=500);
idxT6=find(T>500&T<=1000);
idxT7=find(T>1000);

p1=[0.00000E+00	4.91000E+01	0.00000E+00	6.40000E+01	0.00000E+00];
p2=[0.00000E+00	1.62400E+01	0.00000E+00	9.28000E+02	0.00000E+00];
p3=[-2.17700E-01	1.19838E+01	5.53710E+02	-7.84610E+03	4.13830E+04];
p4=[-4.82000E-03	2.97600E+00	-7.16300E+02	8.30220E+04	-1.53000E+06];
p5=[-6.29000E-05	9.29600E-02	-5.16600E+01	1.37060E+04	1.24000E+06];
p6=[0.00000E+00	0.00000E+00	-2.57000E-01	9.55500E+02	2.45000E+06];
p7=[0 0 0 0 3.14850E+06];

cpNbTi(idxT1)= p1(1)*T(idxT1).^4+p1(2)*T(idxT1).^3+p1(3)*T(idxT1).^2+p1(4)*T(idxT1)+p1(5);
cpNbTi(idxT2)= p2(1)*T(idxT2).^4+p2(2)*T(idxT2).^3+p2(3)*T(idxT2).^2+p2(4)*T(idxT2)+p2(5);
cpNbTi(idxT3)= p3(1)*T(idxT3).^4+p3(2)*T(idxT3).^3+p3(3)*T(idxT3).^2+p3(4)*T(idxT3)+p3(5);
cpNbTi(idxT4)= p4(1)*T(idxT4).^4+p4(2)*T(idxT4).^3+p4(3)*T(idxT4).^2+p4(4)*T(idxT4)+p4(5);
cpNbTi(idxT5)= p5(1)*T(idxT5).^4+p5(2)*T(idxT5).^3+p5(3)*T(idxT5).^2+p5(4)*T(idxT5)+p5(5);
cpNbTi(idxT6)= p6(1)*T(idxT6).^4+p6(2)*T(idxT6).^3+p6(3)*T(idxT6).^2+p6(4)*T(idxT6)+p6(5);
cpNbTi(idxT7)= p7(1)*T(idxT7).^4+p7(2)*T(idxT7).^3+p7(3)*T(idxT7).^2+p7(4)*T(idxT7)+p7(5);

end