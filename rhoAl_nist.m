function rhoAl = rhoAl_nist(T)
% From NIST, 1984, Temperature range: 1-500 K
    a=0.476050395713888;
    b=-3.299730411832043;
    c=7.489576522510857;
    d=-5.738194411483883;
    e=1.121748209742612;
    f=0.207979301275814;
    g=-4.010635050869012;

    logT=log10(T);
    logRho=polyval([a b c d e f g],logT);
    rhoAl=1E-8*10.^logRho;
end