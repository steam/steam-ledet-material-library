function Jc_T_B = Jc_T_B_BSCCO2212_block20T_v2(T,B,f_scaling)
% Critical current density in a BSCCO2112 strand
% Fit proposed by T. Shen
% Experimental data from A. Gosh
% Function by E. Ravaioli
% (C) Emmanuele Ravaioli
% LBNL, Berkeley, CA

if size(T,1)==size(B,1)&size(T,2)==size(B,2)
    Jc_T_B=zeros(size(T));
elseif size(T,1)==size(B,2)&size(T,2)==size(B,1)
    Jc_T_B=zeros(size(T));
    B=B';
else
    if size(B)==[1,1]
        B=ones(size(T))*B;
        Jc_T_B=zeros(size(T));
    elseif size(T)==[1,1]
        T=ones(size(T))*T;
        Jc_T_B=zeros(size(T));
    else
        error('T and B must be scalars or vectors with the same number of elements.')
    end
end

% % % Modify the input magnetic field
B(B<0)=-B(B<0); % direction of the magnetic field is unimportant
B(abs(B)<.195)=.195; % very small magnetic field causes numerical problems

% % % Critical temperature, with B here defined as the field at which Jc is nearly zero (Using Arup Ghosh data)
a1=2463.4;
a2=2.05;
Tc_B=exp((log(a1)-log(B))/a2); % [K]

T0=4.2; % [K] Reference temperature
% B0=[1 3 5 8 10 12 14];
% Jc0=[841	618.2	568	500.67	482.3	458.2	438.11];
% Fit based on the data above (round wire, 1.2 mm diameter, 25% of superconductor)
b1=816.11/(pi/4*.0012^2*.25);
b2=147.9/(pi/4*.0012^2*.25);
Jc_T0_B=b1-b2*log(B); % [A/m^2]

Jc_T_B=Jc_T0_B.*(Tc_B-T)./(Tc_B-T0); % [A/m^2]

% Nonreal negative values
Jc_T_B(Jc_T_B<0)=0; 

% % % Scale this fit 
Jc_T_B=Jc_T_B*f_scaling; % [A/m^2]

end