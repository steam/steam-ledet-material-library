function rhoAg = rhoAg_mat(T)
% range (K)	1-300

    rhoAg=zeros(size(T));
    
    LimitValidityLow=20; % K
    LimitValidityHigh=300; % K
    
    idxT1=find(T<LimitValidityLow);
    idxT2=find(T>=LimitValidityLow&T<=LimitValidityHigh);
	idxT3=find(T>LimitValidityHigh);
    
    fit_rho_Ag_RRR30_B0=[18.7845859965641,-259.111519891231,1518.56183566522,-4897.68625090004,9379.78164848381,-10656.5959438652,6645.34073216796,-1754.20185265205];
    fit_rho_Ag_RRR30_B0_above300K=[1.146746158153352  -1.620011679078631];

    logLimitValidityLow=log10(LimitValidityLow);
    logkLimitValidityLow=polyval(fit_rho_Ag_RRR30_B0,logLimitValidityLow);
    rhoAg(idxT1)=10^logkLimitValidityLow;
    
    logT=log10(T(idxT2));
    logRho=polyval(fit_rho_Ag_RRR30_B0,logT);
    rhoAg(idxT2)=10.^logRho;
    
    logLimitValidityHigh=log10(T(idxT3));
    logkLimitValidityHigh=polyval(fit_rho_Ag_RRR30_B0_above300K,logLimitValidityHigh);
    rhoAg(idxT3)=10.^logkLimitValidityHigh;
    
    rhoAg=rhoAg/10^9;
    
end