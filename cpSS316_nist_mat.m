function cpSS316 = cpSS316_nist_mat(T)
% Data range 4-300
% http://cryogenics.nist.gov/MPropsMAY/316Stainless/316Stainless_rev.htm
   density = 7990; % density not available in NIST database
   
    cpSS316=zeros(size(T));

    LimitValidityLow=4; % K
    LimitValidityHigh=300; % K
    
    idxT1=find(T<LimitValidityLow);
    idxT2=find(T>=LimitValidityLow&T<=50);
    idxT3=find(T>50&T<=LimitValidityHigh);
	idxT4=find(T>LimitValidityHigh);

    fit_cp_SS316_2=[-1.91368 8.44996 3.15315 -89.9982 239.5296 -308.854 218.743 -80.6422 12.2486];
    fit_cp_SS316_3=[17.05262 -237.22704 1382.4627 -4305.7217 7437.6247 -6176.028 76.70125 3643.198 -1879.464];
    fit_cp_SS316_AboveValidityHigh=[0.617209868500000 3.050748366965554E2];

    logLimitValidityLow=log10(LimitValidityLow);
    logkLimitValidityLow=polyval(fit_cp_SS316_2,logLimitValidityLow);
    cpSS316(idxT1)=10^logkLimitValidityLow;
    
    logT2=log10(T(idxT2));
    logCp2=polyval(fit_cp_SS316_2,logT2);
    cpSS316(idxT2)=10.^logCp2;
    
    logT3=log10(T(idxT3));
    logCp3=polyval(fit_cp_SS316_3,logT3);
    cpSS316(idxT3)=10.^logCp3;
    
    cpSS316(idxT4)=polyval(fit_cp_SS316_AboveValidityHigh,T(idxT4));
    
    cpSS316 = density*cpSS316;
end
