function cpG10 = cpG10_nist_mat(T)
% from NIST
% Validity range: 4-300 K
% Validity range arbitrarily extended in this function

    density = 1900; % [kg/m^3]
    LimitValidityLow=1.8; % K
    LimitValidityHigh=500; % K

    fit_cp_G10_2=[0.015236 -0.24396 1.4294 -4.2386 7.3301 -8.2982 7.6006 -2.4083];
%     fit_cp_G10_AboveValidityHigh=[ 3.249893866039781   24.828381119571091];

    cpG10=zeros(size(T));

    idxT1=find(T<LimitValidityLow);
    idxT2=find(T>=LimitValidityLow&T<=LimitValidityHigh);
    idxT3=find(T>LimitValidityHigh);

    logLimitValidityLow=log10(LimitValidityLow);
    logkLimitValidityLow=polyval(fit_cp_G10_2,logLimitValidityLow);
    cpG10(idxT1)=10^logkLimitValidityLow;

    logT2=log10(T(idxT2));
    logCp2=polyval(fit_cp_G10_2,logT2);
    cpG10(idxT2)=10.^logCp2;

%     cpG10(idxT3)=polyval(fit_cp_G10_AboveValidityHigh,T(idxT3));
    logLimitValidityHigh=log10(LimitValidityHigh);
    logkLimitValidityHigh=polyval(fit_cp_G10_2,logLimitValidityHigh);
    cpG10(idxT3)=10^logkLimitValidityHigh;

    cpG10 = density*cpG10;
end
