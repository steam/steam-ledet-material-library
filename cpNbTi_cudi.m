function cpNbTi = cpNbTi_cudi(T)
% Temperature range: 1-500 K
% The non-linear jump in the heat capacity which occurs at the critical is
% here assumed to always occur at T=6 K
Tc=6;

if T<Tc
    p=[0.00000E+00	4.91000E+01	0.00000E+00	6.40000E+01	0.00000E+00];
elseif T<20
    p=[0.00000E+00	1.62400E+01	0.00000E+00	9.28000E+02	0.00000E+00];
elseif T<50
    p=[-2.17700E-01	1.19838E+01	5.53710E+02	-7.84610E+03	4.13830E+04];
elseif T<175
    p=[-4.82000E-03	2.97600E+00	-7.16300E+02	8.30220E+04	-1.53000E+06];
elseif T<500
    p=[-6.29000E-05	9.29600E-02	-5.16600E+01	1.37060E+04	1.24000E+06];
elseif T<1000
    p=[0.00000E+00	0.00000E+00	-2.57000E-01	9.55500E+02	2.45000E+06];
else
    p=[0 0 0 0 3.14850E+06];
end
a=p(1);
b=p(2);
c=p(3);
d=p(4);
e=p(5);

cpNbTi= a*T^4+b*T^3+c*T^2+d*T+e;
end