function kKapton = kKapton(T)
% Function to calculate kapton thermal conductivity
% T in [K]
% k in [W/K/m]
% Range	of validity:  1-500 K
% E. Ravaioli, TE-MPE, CERN

if T<4.3
    kKapton=.010703-.00161*(4.3-T);
else
    a=5.73101;
    b=-39.5199;
    c=79.9313;
    d=-83.8572;
    e=50.9157;
    f=-17.9835;
    g=3.42413;
    h=-0.27133;
    logT=log10(T);
    logk=a+b*logT+c*logT^2+d*logT^3+e*logT^4+f*logT^5+g*logT^6+h*logT^7;
    kKapton=10^logk;
end