function [Ic_sc, Jc_sc] = Jc_Nb3Sn_Bottura(T, B, fitParameters, wireParameters)
% Critical current density in a Nb3Sn wire/strand
% Calculation follows the fit proposed by L. Bottura. Note: In this simplified implementation the effect of strain is ignored
% Reference: L. Bottura and B. Bordini, "JC(B,T,?) Parameterization for the ITER Nb3Sn Production," in IEEE Transactions on Applied Superconductivity, vol. 19, no. 3, pp. 1521-1524, June 2009, doi: 10.1109/TASC.2009.2018278.
% Function written by E. Ravaioli
% July 2022
% (C) Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH
% 
% The function calculates the critical current and critical current density
% in the superconductor either for one wire/strands or more than one
% simulateneously.
% 
% Note: The minimum accepted value of B is 1 uT: lower values will be set to 1 uT.
% 
% Fit:
% Jc_sc(T,B,eps)   = CJ / B * s(eps) * (1-t^1.52) * (1-t^2) * b^p * (1-b)^q
% where b = B / Bc2(T,eps)
% where t = T / Tc(B=0,eps)
% where Bc2(T,eps) = Bc20 * s(eps) * (1 - t^1.52)
% where Tc(B,eps)  = Tc0 * (s(eps))^(1/3) * (1 - (B/Bc2(T=0,eps))^(1/1.52)
% where s(eps) is the strain function, which is set to 1 (hence ignoring strain dependance) in this simplified fit function
% Note that if s(eps)=1 then t=T/Tc0
% Inputs:
% - T: Temperature (scalar or vector) [K]
% - B: Magnetic field (scalar or vector) [T]
% - Structure defining fit parameters (scalars or vectors), OR matrix with parameters
% following this order (columns are different strands, rows are different parameters):
% -- Tc0: Critical temperature at B=0, I=0 [K]
% -- Bc20: Critical magnetic field at T=0, I=0 [T]
% -- CJ: Parameter for the fit [A/m^2]                TO DO
% -- p: Parameter for the fit [-]             TO DO
% -- q: Parameter for the fit [-]              TO DO
% - Structure defining wire/strand parameters (scalars or vectors), OR matrix with parameters
% following this order (columns are different strands, rows are different parameters):
% -- wireDiameter: Wire/strand diameter [m]
% -- Cu_noCu: Copper to non-Copper ration in the wire/strand [-]
% Outputs:
% - Ic_sc: Critical current in the wire/strand (scalars or vectors) [A]
% - Jc_sc: Critical current density in the superconductor (scalars or
% vectors) [A/m^2]


% % % Exponential factor appearing in the fit (hard-coded)
exp_factor = 1.52;

% % % Unpack parameters
if isstruct(fitParameters)==1
    Tc0    = fitParameters.Tc0;
    Bc20   = fitParameters.Bc20;
    CJ     = fitParameters.CJ;
    p     = fitParameters.p;
    q      = fitParameters.q;
else
    Tc0    = fitParameters(1,:);
    Bc20   = fitParameters(2,:);
    CJ     = fitParameters(3,:);
    p     = fitParameters(4,:);
    q      = fitParameters(5,:);
end
if isstruct(wireParameters)==1
    wireDiameter = wireParameters.wireDiameter;
    Cu_noCu = wireParameters.Cu_noCu;
else
    wireDiameter = wireParameters(1,:);
    Cu_noCu = wireParameters(2,:);
end
f_sc = 1 ./ (1+Cu_noCu);

% % % Check all inputs are scalars or vectors
if ( isvector(T) && isvector(B) && isvector(Tc0) && isvector(Bc20) && isvector(CJ) && isvector(p) && isvector(q) )~=1
    error('All inputs must be scalars or vectors with the same number of elements.')
end

nElements=max([ numel(T), numel(B), numel(Tc0), numel(Bc20), numel(CJ), numel(p), numel(q) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (...
        numel(T)>1  && numel(T)~=nElements  ||...
        numel(B)>1  && numel(B)~=nElements  ||...
        numel(Tc0)>1   && numel(Tc0)~=nElements   ||...
        numel(Bc20)>1  && numel(Bc20)~=nElements  ||...
        numel(CJ)>1 && numel(CJ)~=nElements ||...
        numel(p)>1 && numel(p)~=nElements ||...
        numel(q)>1 && numel(q)~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end

% % % Find whether the inputs are given as vertical or horizontal vectors
% Note: iscolumn returns 1 in case of a scalar
if iscolumn(T)~=0||iscolumn(B)~=0||iscolumn(Tc0)~=0||iscolumn(Bc20)~=0
    flagColumn=0;
else
    flagColumn=1;
end

% % % Make vectors if T, B, Tc0, Bc20 are scalar
if flagColumn==1
    if numel(T)<nElements
        T = T*ones(nElements,1);
    end
    if numel(B)<nElements
        B = B*ones(nElements,1);
    end
    if numel(Tc0)<nElements
        Tc0 = Tc0*ones(nElements,1);
    end
    if numel(Bc20)<nElements
        Bc20 = Bc20*ones(nElements,1);
    end
elseif flagColumn==0
    if numel(T)<nElements
        T = T*ones(1,nElements);
    end
    if numel(B)<nElements
        B = B*ones(1,nElements);
    end
    if numel(Tc0)<nElements
        Tc0 = Tc0*ones(1,nElements);
    end
    if numel(Bc20)<nElements
        Bc20 = Bc20*ones(1,nElements);
    end
end

% % % Set minimum B
minB = 1e-6; % [T]

% % % Calculate absolute magnetic field [field polarity not important)
B = max(minB, abs(B));

% % % Calculate reduced temperature, i.e. ratio between T and T_c0
% Reminder: This step is only correct when ignoring the strain dependance
t=T./Tc0;
t(t>1)=1; % avoid values higher than 1

% % % Calculate critical magnetic field [corrected to avoid Bc2=0], and the reduced magnetic field
% Reminder: This step is only correct when ignoring the strain dependance
Bc2 = max(1e-9, Bc20.* (1 - t.^exp_factor));
b=B./Bc2;
b(b>1)=1; % avoid values higher than 1

% % % Calculate critical current density
Jc_sc = CJ ./B .* b.^p .* (1-t.^2) .* (1-t.^exp_factor) .* (1 - b).^q;

% % % Set to 0 the critical current density if T>=Tc0
Jc_sc(T>=Tc0) = 0;

% % % Set to 0 the critical current density if B>=Bc20
Jc_sc(B>=Bc20) = 0;

% % % Set to 0 the critical current if calculation yields a  negative value
Jc_sc(Jc_sc<0) = 0;

% % % Calculate critical current
Ic_sc = Jc_sc .* (pi/4 * wireDiameter.^2 .* f_sc);

end