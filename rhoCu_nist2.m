function rhoCu = rhoCu_nist2(T,B,RRR,f_MR)

rhoCu=zeros(size(T));

if nargin<4
    f_MR=1; % default, not change of the magneto-ressitivity
end

if size(RRR)==[1,1]
    RRR=ones(size(T))*RRR;
end
if size(B)==[1,1]
    B=ones(size(T))*B;
end

idxLowB=find(B<0.1);
idxHighB=find(B>=0.1);

rho0=1.553e-8./RRR;
rhoi=1.171e-17*(T.^4.49)./(1+4.48e-7*(T.^3.35).*exp(-(50./T).^6.428));
% rhoi=1.171e-17*(T.^4.49)./(1+1.171e-17*3.841e10*(T.^(4.49+1.14)).*exp(-(50./T).^6.428));
rhoiref=0.4531*rho0.*rhoi./(rho0+rhoi);
rhcu=rho0+rhoi+rhoiref;

rho0_273K=1.553e-8./RRR;
rhoi_273K=1.171e-17*(273.^4.49)./(1+4.48e-7*(273.^3.35).*exp(-(50./273).^6.428));
rhoiref_273K=0.4531*rho0_273K.*rhoi_273K./(rho0_273K+rhoi_273K);
rhcu_273K=rho0_273K+rhoi_273K+rhoiref_273K;

% Elements with very low magnetic field (B<0.1 T)
rhoCu(idxLowB)=rhcu(idxLowB);

% Other elements (B>=0.1 T)
lgs=0.43429*log(1.553D-8*B(idxHighB)./rhcu(idxHighB));
lgs=log10(B(idxHighB).*(rhcu_273K(idxHighB)./rhcu(idxHighB)));
% polys=-2.662+lgs.*(0.3168+lgs.*(0.6229+lgs.*(-0.1839+lgs*0.01827)));
polys=-2.662+0.3168*lgs+0.6229*lgs.^2-0.1839*lgs.^3+0.01827*lgs.^4;
corrs=(10.^polys);
rhoCu(idxHighB)=(1.+corrs*f_MR).*rhcu(idxHighB);

end

% % % % Old version (only one element at a time)
% function rhoCu = rhoCu_nist(T,B,RRR)
%    rho0=1.553e-8/RRR;
%    rhoi=1.171e-17*(T^4.49)/(1+4.48e-7*(T^3.35)*exp(-(50/T)^6.428));
%    rhoiref=0.4531*rho0*rhoi/(rho0+rhoi)  ;    
%    rhcu=rho0+rhoi+rhoiref;
%    if (B<0.1)
%       rhoCu=rhcu;
%    else
%       lgs=0.43429*log(1.553D-8*B/rhcu);
%       polys=-2.662+lgs*(0.3168+lgs*(0.6229+lgs*(-0.1839+lgs*0.01827)));
%       corrs=(10^polys);
%       rhoCu=(1.+corrs)*rhcu;
%    end
% end
