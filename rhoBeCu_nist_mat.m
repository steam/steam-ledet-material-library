function rhoBeCu = rhoBeCu_nist_mat(T, f_scaling)
    % Electrical Resistivity Dependence of C17200 Beryllium Copper on Test Temperature (4-300 K).
    % Source: N. J. Simon, �. S. Drexler, and R. P. Reed, "Properties of Copper and Copper Alloys at Cryogenic Temperatures", NIST Monograph 177, 1992
    % https://nvlpubs.nist.gov/nistpubs/Legacy/MONO/nistmonograph177.pdf
    % 
    % Function by E. Ravaioli, TE-MPE-PE, CERN, Geneva, CH
    % August 2021
    
    % Scaling factor (optional)
    if nargin<2
        f_scaling=1;
    end

    
    % Measurement data
    T_meas = [4 19.6 75.8 192 273];
%     rho_el_meas = [ 82.1 81.8 84.9 96.8 105.1]/1e9; % original
    rho_el_meas = [82 82 84.9 96.8 105.1 ]/1e9; % adjusted to enforce monotonic increase
        
    rhoBeCu = interp1(T_meas, rho_el_meas, T,'linear', 'extrap');
    
    
    % Scale resistivity with a factor
    rhoBeCu=f_scaling*rhoBeCu;
end