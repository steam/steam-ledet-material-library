function cpAl = cpAl5083_nist(T)
%  5083 Aluminum (UNS A95083)
% Temperature range: 4-300 K
   density=2699;
   a=2.96344;
   b=-38.3094;
   c=210.351;
   d=-637.795;
   e=1162.27;
   f=-1298.3;
   g=866.662;
   h=-314.292;
   i=46.6467;

   if T<4
       T=4;
   elseif T>300
       T=300;
   end
    logT=log10(T);
    logcpAl=polyval([a b c d e f g h i],logT);
    cpAl=density*10^logcpAl;
end