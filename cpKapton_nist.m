function cpKapton = cpKapton_nist(T)
% Volumetric specific heat of kapton
% Input: Temperature [K]
% Output: Volumetric specific heat [J/m^3/K]
   density=1420;
   a0=-1.3684;
   a1=0.65892;   
   a2=2.8719;
   a3=0.42651;
   a4=-3.0088;
   a5=1.9558;
   a6=-0.51998;
   a7=0.051574;
   logT=log10(T);
   p=10^(a7*((logT)^7)+a6*((logT)^6)+a5*((logT)^5)+a4*((logT)^4)+a3*((logT)^3)+a2*((logT)^2)+a1*((logT))+a0);   
   cpKapton=density*p;
end