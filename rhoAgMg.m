function rhoAgMg = rhoAgMg(tt,bb)

% Function Written by Lucas Brouwer
% Lawrence Berkeley National Laboratory
% Modified by Daniel Davis
% National High Magnetic Field Laboratory

% Some data for the resistivity of Ag-0.2 wt%Mg is given in the following reference [3]. 
% The variation of RRR~5.1 Ag-Mg was fit vs. temperature using similar approach to Ag. 
% From looking at the Kohler plot, the variation with magnetic field appears to be similar 
% enough to that of Ag, that the same coefficients for the magnetic field variation was used as Ag.
% The temperature fit follows the same approach as the nist copper fit with P7 set to zero.
% The fitted parameters and function is below. 

% [3] P. Li et al. "RRR and thermal conductivity of Ag and Ag-0.2 wt%Mg alloy in Ag/Bi-2212 wires�, 
% IOP Conf. Series: Materials Science and Engineering 102 (2015) 012027




if size(tt,1)==size(bb,1)&size(tt,2)==size(bb,2)
    rhoAgMg=zeros(size(tt));
elseif size(tt,1)==size(bb,2)&size(tt,2)==size(bb,1)
    rhoAgMg=zeros(size(tt));
    bb=bb';
else
    if size(bb)==[1,1]
        bb=ones(size(tt))*bb;
        rhoAgMg=zeros(size(tt));
    elseif size(tt)==[1,1]
        tt=ones(size(bb))*tt;
        rhoAgMg=zeros(size(bb));
    else
        error('T and B must be scalars or vectors with the same number of elements.')
    end
end



    %%% RRR is assumed to 5.13 (from Liyang paper)
    rho_0 = 3.84e-9;
    c0 =  1.97e-8;    %resistivity at 300 K  

    
 %     Constants
    P1 = 3.51121e-15;   
    P2 = 3.40152;
    P3 = 2.04974e10;    
    P4 = 1.01777;    
    P5 = 40.0;  
    P6 = 3.16452;   
    P7 = 0;   
 
    a0 = -4.36736;
    a1 = 2.31218;
    a2 = -0.0966798;
    a3 = -0.0931658;
    a4 = 0.0151959;

    
    b=bb;
%    b(b<0)=-b(b<0); % direction of the magnetic field is unimportant
    b=abs(bb);
    
    T = tt;
 
  
for t=1:length(rhoAgMg)

%     rho_0 = c0_scale*c0/rrr; 
    rho_i = P1*(T(t).^P2)./(1+P1*P3.*(T(t).^(P2-P4)).*exp(-((P5./T(t)).^P6)));
    rho_i0 = P7*rho_i*rho_0/(rho_i+rho_0);
    rho_n = rho_0+rho_i+rho_i0;

    if (b(t) > 0.01d0)
        x = c0 .* b(t) ./ rho_n;
        log_x = log10(x);
        f_exp = a0+a1*log_x+a2*(log_x^2)+a3*(log_x^3) + a4*(log_x^4);
        corr = 10.0d0^f_exp;
    else
        corr=0.0d0;
    end

    rhoAgMg(t) =rho_n*(1+corr);
end

end
 
