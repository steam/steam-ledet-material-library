function cpKapton = cpKapton_nist_mat(T)
%     Source http://cryogenics.nist.gov/NewFiles/Polyimide_kapton.html
%     Valid from 4 to 300 K
%     log Cp = a + b(logT) + c(logT)2 + d(logT)3 + e(logT)4 + f(logT)5 + g(logT)6 + h(logT)7
%
%     Function by E. Ravaioli, CERN, Geneva, CH
%     May 2019

    density=1420; % [kg/m^3]
    LimitValidityLow=1.9; % [K]
    
    a0=-1.3684;
    a1=0.65892;
    a2=2.8719;
    a3=0.42651;
    a4=-3.0088;
    a5=1.9558;
    a6=-0.51998;
    a7=0.051574;
    fit_cp_kapton=[a7 a6 a5 a4 a3 a2 a1 a0];
    
    p=zeros(size(T));
    idxT1=T<LimitValidityLow;
    idxT2=T>=LimitValidityLow;
    
    logT=log10(LimitValidityLow);
    p(idxT1)=10.^(a7*((logT).^7)+a6*((logT).^6)+a5*((logT).^5)+a4*((logT).^4)+a3*((logT).^3)+a2*((logT).^2)+a1*((logT))+a0);
    
    logT=log10(T(idxT2));
    logCp2=polyval(fit_cp_kapton,logT);
    p(idxT2)=10.^logCp2;
    
    cpKapton=density*p;
end