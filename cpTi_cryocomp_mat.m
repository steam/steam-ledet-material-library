function cpTi = cpTi_cryocomp_mat(T)
% from Cryocomp

    density = 4540; % [kg/m^3]
    LimitValidityLow=4; % K
    LimitValidityHigh=300; % K
    
    fit_cp_Ti_2=[-0.576099035551066   6.118441661578747 -26.439181574747003  59.954465708087419 -77.256966475288834  56.803684410219617 -19.144696871373529   0.552095810764055];
    fit_cp_Ti_AboveValidityHigh=[ 0.4493506493507   403.8787878787871];

    lengthT=length(T);

    if lengthT==1
        if T<LimitValidityLow
            logLimitValidityLow=log10(LimitValidityLow);
            logkLimitValidityLow=polyval(fit_cp_Ti_2,logLimitValidityLow);
            cpTi=10^logkLimitValidityLow;
        elseif T<=LimitValidityHigh
            logT2=log10(T);
            logCp2=polyval(fit_cp_Ti_2,logT2);
            cpTi=10.^logCp2;
        else
            cpTi=polyval(fit_cp_Ti_AboveValidityHigh,T);
        end
    else
        cpTi=zeros(lengthT,1);

        idxT1=find(T<LimitValidityLow);
        idxT2=find(T>=LimitValidityLow&T<=LimitValidityHigh);
        idxT3=find(T>LimitValidityHigh);

        logLimitValidityLow=log10(LimitValidityLow);
        logkLimitValidityLow=polyval(fit_cp_Ti_2,logLimitValidityLow);
        cpTi(idxT1)=10^logkLimitValidityLow;

        logT2=log10(T(idxT2));
        logCp2=polyval(fit_cp_Ti_2,logT2);
        cpTi(idxT2)=10.^logCp2;

        cpTi(idxT3)=polyval(fit_cp_Ti_AboveValidityHigh,T(idxT3));
    end
    cpTi = density*cpTi;
end
