function [Tc, Tcs]= Tc_Tcs_Nb3Sn_approx(J_sc, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn)
% Calculate approximated critical temperature and current sharing temperature in a Nb3Sn
% conductor.
% Critical current density at T=0, B=0 based on Summer's fit
% 
% See Tc_Tcs_Nb3Sn_Summer for a slower, more accurate function.
% 
% Input J: scalar or vector, Current density in the superconductor [A/m^2]
% Input B: scalar or vector, Magnetic field [T]
% Input Jc_Nb3Sn0: scalar or vector, Parameter of critical current density in Summer's fit [A*T^0.5/m^2]
% Input Tc0_Nb3Sn: scalar or vector, Critical current at B=0 [K]
% Input Bc20_Nb3Sn: scalar or vector, Critical magnetic field at T=0 [T]
% 
% Output Tc: scalar or vector, critical temperature (at J=0)
% Output Tcs: scalar or vector, current sharing temperature
% 
% Function by E. Ravaioli
% (C) Emmanuele Ravaioli, 2019
% CERN, Geneva, CH


% % % Find size to use for the output of the function
if numel(J_sc)>1
    outputSize=size(J_sc);
elseif numel(B)>1
    outputSize=size(B);
end

% % % Check all inputs are scalars or vectors with the same number of elements
if ...
        (numel(J_sc)>1 && ~isequal(size(J_sc),outputSize)) ||...
        (numel(B)>1 && ~isequal(size(B),outputSize)) ||...
        (numel(Jc_Nb3Sn0)>1 && ~isequal(size(Jc_Nb3Sn0),outputSize)) ||...
        (numel(Tc0_Nb3Sn)>1 && ~isequal(size(Tc0_Nb3Sn),outputSize)) ||...
        (numel(Bc20_Nb3Sn)>1 && ~isequal(size(Bc20_Nb3Sn),outputSize))
    error('All inputs must be scalars or vectors with the same number of elements.')
end


% Make all values of J_sc and B are positive (direction of current and magnetic field is unimportant)
J_sc=abs(J_sc);
B=abs(B);

% Simplified Tc calculation
f_B_Bc2=B./Bc20_Nb3Sn;
f_B_Bc2(f_B_Bc2>1)=1; % avoid values higher than 1
Tc=Tc0_Nb3Sn.*(1-f_B_Bc2).^.59;

% Simplified Tcs calculation
Jc0=Jc_Nb3Sn_Summer(zeros(size(B)), B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn);
f_J_Jc0=J_sc./Jc0;
f_J_Jc0(f_J_Jc0>1)=1; % avoid values higher than 1
Tcs=(1-f_J_Jc0).*Tc;

end