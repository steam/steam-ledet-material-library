function cpSS316 = cpSS316_nist(T)
% Data range 4-300
% http://cryogenics.nist.gov/MPropsMAY/316Stainless/316Stainless_rev.htm
   density = 7990; % density not available in NIST database
   if T<=50
       a0=12.2486;
       a1=-80.6422;   
       a2=218.743;
       a3=-308.854;
       a4=239.5296;
       a5=-89.9982;
       a6=3.15315;
       a7=8.44996;
       a8=-1.91368;
   else
       a0=-1879.464;
       a1=3643.198;   
       a2=76.70125;
       a3=-6176.028;
       a4=7437.6247;
       a5=-4305.7217;
       a6=1382.4627;
       a7=-237.22704;
       a8=17.05262;
   end
   logT=log10(T);
   p=10^(a8*logT^8+a7*logT^7+a6*logT^6+a5*logT^5+a4*logT^4+a3*logT^3+a2*logT^2+a1*logT+a0);
   cpSS316 = density*p;
end

% fit_cp_SS316_3=[17.05262 -237.22704 1382.4627 -4305.7217 7437.6247 -6176.028 76.70125 3643.198 -1879.464];