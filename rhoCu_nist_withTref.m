function [rhoCu, f_correction_RRR] = rhoCu_nist_withTref(T, B, RRR, T_ref_high, T_ref_low, f_MR)
% This function calls the NIST Cu electrical resistivity function after
% correcting the error on the RRR due to different measurement temperature.
% Note: NIST fit defines the RRR between 273 K and 4 K.
% Input:
% - T: Temperature [K]
% - B: Magnetic field [T]
% - RRR: Residual Resistivity Ratio [-]
% - T_ref_high [K] and T_ref_low [K] define the two temperatures at which 
% the RRR was measured.
% - f_MR is a flag that can be used to scale the magneto-resistivity (NOT RECOMMENDED)
% Output:
% - rhoCu: Electrical resistivity [Ohm*m]
% - f_correction_RRR: Adopted RRR correction factor [-]
% 
% % % (C) Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH

if nargin<6
    f_MR=1; % default, no change of the magneto-ressitivity
end

% % % Hard-coded temperatures at which the RRR used in the NIST fit is defined
T_NIST_high = 273; % [K]
T_NIST_low  =   4; % [K]

% % % If RRR is given as a vector and T and B are not, make T and B vectors with as many elements as RRR
if length(RRR)>=length(T)
    if size(T)==[1,1]
        T=ones(size(RRR))*T;
    end
    if size(B)==[1,1]
        B=ones(size(RRR))*B;
    end
end

% % % Calculate the correction factor for the measured RRR
f_correction_RRR = (rhoCu_nist(T_ref_low, 0, RRR) ./ rhoCu_nist(T_NIST_low, 0, RRR)) ./ (rhoCu_nist(T_ref_high, 0, RRR) ./ rhoCu_nist(T_NIST_high, 0, RRR));

% % % Calculate Cu resitivity using NIST fit
rhoCu = rhoCu_nist(T, B, RRR.*f_correction_RRR, f_MR);

end