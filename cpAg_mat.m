function cpAg = cpAg_mat(T)
% Volumetric specific heat of silver
% range (K)	1-300

    cpAg=zeros(size(T));

    density=10470; % [kg/m^3]
    
    LimitValidityLow=1; % K
    LimitValidityHigh=300; % K
    
    idxT1=find(T<LimitValidityLow);
    idxT2=find(T>=LimitValidityLow&T<=LimitValidityHigh);
	idxT3=find(T>LimitValidityHigh);
    
    fit_cp_Ag_RRR30_B0=[0.191049066537241,-2.34359159400347,11.5766999155046,-29.3520406103732,40.4479932778170,-30.2548599606873,12.2576978613138,-0.170606365568724,-2.11971321961447];

    logLimitValidityLow=log10(LimitValidityLow);
    logkLimitValidityLow=polyval(fit_cp_Ag_RRR30_B0,logLimitValidityLow);
    cpAg(idxT1)=10^logkLimitValidityLow;
    
    logT=log10(T(idxT2));
    logRho=polyval(fit_cp_Ag_RRR30_B0,logT);
    cpAg(idxT2)=10.^logRho;
    
    logLimitValidityHigh=log10(LimitValidityHigh);
    logkLimitValidityHigh=polyval(fit_cp_Ag_RRR30_B0,logLimitValidityHigh);
    cpAg(idxT3)=10.^logkLimitValidityHigh;
    
    cpAg=cpAg*density;
    
end