function [thermal_conductivity_ag] = kAg(T)

%Calculation of specific heat conductivity of silver depending on temperature
%in the temperature range between 4K and 300K
%Input: Symbol / Description / Unit
% T / Temperature / K
%Output: Symbol / Description / Unit
% thermal_conductivity_ag / Specific heat conductivity/ W/(m*K)
%Source: R. Smith, F. R. Fickett; Low-Temperature Properties of Silver; J. Res. Natl. Inst.
%Stand. Technol. 100, 119 (1995)
a = [-8.085202766065000e-04, -3.358195377766776e-07, 8.147381572319895e-11];
b = [0.084173230577580, 1.869961406456805e-04, -1.042739292996614e-07];
c = [-2.898387926986296, -0.041125881979616, 5.409027188706845e-05];
d = [30.127572196852682, 4.473201297735611, -0.013972777872369];
e = [1.323932096483007e+02, -2.413050714859127e+02, 1.569131427472265];
f = [1.031106730600989e+02, 5.659032445727915e+03, 4.105421708582979e+02];
for k=1:length(T)
    if T(k) <= 33
        n = 1;
    else
        if T(k) <= 100
            n = 2;
        else
            n = 3;
        end
    end
thermal_conductivity_ag(k) = a(n)*T(k)^5 + b(n)*T(k)^4 + c(n)*T(k)^3 + d(n)*T(k)^2 + e(n)*T(k) + f(n);
end
