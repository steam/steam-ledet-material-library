function rhoCu = rhoCu_nist_cable(T,B,RRR,StrandToCable,f_MR,sparseStrandToCable)
% This function calculates the resistivity of copper based on NIST
% properties.
% Inputs:
% T : Vector of temperatures in each cable
% B : Vector of magnetic field in each strand of each cables
% RRR : Vector of RRR of each cable
% StrandToCable : Vector indicating in which cable each strand is located
% f_MR : (optional) Factor to scale the magneto-resistivity (default=1)
% sparseStrandToHalfTurn: (optional) If this sparse matrix is provided as
% an input, calculation speed is improved by ~25%. The matrix can be simply
% calculated as:
% sparseStrandToHalfTurn=sparse(1:nStrands,StrandToHalfTurn,1);
% See also the function rhoCu_nist, which works with scalars or same-size
% vectors for T, B, RRR.
% 
% E. Ravaioli, CERN, Geneva, CH

% rhoCu=zeros(size(T));

% nCables=length(rhoCu);
nStrands=length(StrandToCable);
if nargin<6
    sparseStrandToCable=sparse(1:nStrands,StrandToCable,1);
end
nS=sum(sparseStrandToCable);

if nargin<5
    f_MR=1; % default, no change of the magneto-ressitivity
end

if size(B)==[1,1]
    B=ones(size(T))*B;
end

% % % Take absolute magnetic field
B=abs(B);

% % % Avoid passing too little magnetic field to the code below
% B(B<.1)=.1;

rho0=1.553e-8./RRR;
rhoi=1.171e-17*(T.^4.49)./(1+4.48e-7*(T.^3.35).*exp(-(50./T).^6.428));
rhoiref=0.4531*rho0.*rhoi./(rho0+rhoi);
rhoCu=rho0+rhoi+rhoiref;

lgs=0.43429*log(1.553D-8*B./rhoCu(StrandToCable));
poly=-2.662+lgs.*(0.3168+lgs.*(0.6229+lgs.*(-0.1839+lgs*0.01827)));
corrs=(10.^poly);

% Only apply magneto-resistivity correction to the strands whose magnetic
% field is higher than 0.1 T (the function diverges for low B)
corrs(B<.1)=0;
% % % Calculate average (this fancy way makes it faster)
corrs=corrs*sparseStrandToCable./nS;

rhoCu=(1.+corrs*f_MR).*rhoCu;

end
