function kCu = kCu_nist(T,B,RRR)
    beta=0.634/RRR;
    beta_r=beta/0.0003;
    P1=1.754e-8;
    P2=2.763;
    P3=1102;
    P4=-0.165;
    P5=70;
    P6=1.756;
    P7=0.838/(beta_r^0.1661);
    W0=beta/T;
% % %     Wi=P1*(T^P2)/(1+P1*P3*T^(P2+P4*exp((-P5/T)^P6))); % OLD, WRONG. Corrected on 2021/08/25
    Wi=P1*(T^P2)/(1+P1*P3*T^(P2+P4)*exp(-(P5/T)^P6));
    Wi0=P7*Wi*W0/(Wi+W0);
    kCu=(1/(W0+Wi+Wi0))*rhoCu_nist(T,0,RRR)/rhoCu_nist(T,B,RRR);
end