function cpNb3Sn = cpNb3Sn_cryocomp(T,Tc,Tc0,Tcs)
% C #  0.. 20 K: V.D. Arp, Stability and Thermal Quenches in Force-Cooled
% C #            Superconducting Cables, Superconducting MHD Magnet Design
% C #            Conference, MIT, pp 142-157, 1980.
% C # 20..400 K: G.S.Knapp,S.D.Bader,Z.Fisk, Phonon properties of A-15
% C #            superconductors obtained from heat capacity measurements,
% C #            Phys.Rev B, 13, no.9, pp 3783-3789, 1976.

   density = 8950.0;
   
   % Normal component
   if T <= 10
       pn=(7.5475e-3)*T^2;
   elseif T <= 20
       pn=(-0.3+0.00375*T^2)/0.09937;
   elseif T <= 400
       AA  =   38.2226876; 
       BB  =   -848.36422; 
       CC  =   1415.13807; 
       DD  =   -346.83796;  
       a   =  6.804586085; 
       b   =  59.92091818;
       c   =  25.82863336; 
       d   =  8.779183354; 
       na  =   1;          
       nb  =   2;          
       nc  =   3;          
       nd  =   4;           
       pn = AA*T/(a+T)^na+BB*T^2/(b+T)^nb + CC*T^3/(c+T)^nc+DD*T^4/(d+T)^nd;   
   else      
       pn = 250.8246; 
   end  
   % Superconducting component
   if T<Tc
       TT=Tc/Tc0;
       dBc2Dt=-0.46306-0.067830*Tc;
       delcp=1500*(dBc2Dt^2)/(2*(27.2/(1+0.34*TT))^2-1);
       if Tc<=10
           cpntc=7.5475e-3*Tc^2;
       elseif Tc<=20
           cpntc=(-0.3+0.00375*Tc^2)/0.09937;
       end
       ps=(cpntc+delcp)*(T/Tc)^3;
   else
       ps=0;
   end
   if T<Tcs
       p=ps;
   elseif T<Tc
       F=(T-Tcs)/(Tc-Tcs);
       p=F*pn+(1-F)*ps;
   else
       p=pn;
   end;
   cpNb3Sn=density*p;
end