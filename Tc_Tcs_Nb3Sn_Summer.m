function [Tc, Tcs]= Tc_Tcs_Nb3Sn_Summer(J, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn)
% Calculate critical temperature and current sharing temperature in a Nb3Sn strand, using
% Summer's fit
% 
% Output Tc: scalar, critical temperature (at J=0)
% Output Tcs: scalar or vector, current sharing temperature
% 
% Function by E. Ravaioli
% (C) Emmanuele Ravaioli, 2019
% CERN, Geneva, CH

% % % Check all inputs are scalars or vectors
if ( isvector(J) && isvector(B) && isvector(Jc_Nb3Sn0) && isvector(Tc0_Nb3Sn) && isvector(Bc20_Nb3Sn) )~=1
    error('All inputs must be scalars or vectors with the same number of elements.')
end

nElements=max([ length(J(:)), length(B(:)), length(Jc_Nb3Sn0(:)), length(Tc0_Nb3Sn(:)), length(Bc20_Nb3Sn(:)) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (length(J(:))>1 && length(J(:))~=nElements) || (length(B(:))>1 && length(B(:))~=nElements) ||...
        (length(Jc_Nb3Sn0(:))>1 && length(Jc_Nb3Sn0(:))~=nElements) ||...
        (length(Tc0_Nb3Sn(:))>1 && length(Tc0_Nb3Sn(:))~=nElements) ||...
        (length(Bc20_Nb3Sn(:))>1 && length(Bc20_Nb3Sn(:))~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end

J(J<0)=-J(J<0); % direction of the current is unimportant

% % % Vector with temperatures T to try
Tmin=.1;
Tmax=18;
Tstep=.1;
try_T=Tmin:Tstep:Tmax;

% % % Calculate Jc(T,B) at the input magnetic field B and varying temperature T
tryJc_T=Jc_Nb3Sn_Summer(try_T, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn);

% % % Find current sharing temperature (old code, poor performance)
% Tcs=zeros(nElements,1);
% for s=1:length(J)
%     idxTcs=find(tryJc_T<=J(s),1)-1;
%     
%     if idxTcs~=0
%         Tcs(s)=try_T(idxTcs);
%     else
%         Tcs(s)=Tmin;
%     end
% end

% % % Add one element to allow extrapolation
% tryJc_T(length(tryJc_T)+1)=1E100;
% try_T(length(try_T)+1)=Tmin;

% % % Eliminate non-unique elements (needed to use interp1)
[tryJc_T, index] = unique(tryJc_T);


if tryJc_T==0
    Tc=0;
    Tcs=0;
else
    % % % Find critical temperature (for J=0)
    Tc=interp1(tryJc_T,try_T(index), 0, 'linear',Tmin);

    % % % Find current sharing temperature
    Tcs=interp1(tryJc_T,try_T(index), J, 'linear',Tmin);
end

end