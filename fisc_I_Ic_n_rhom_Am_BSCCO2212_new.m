function f_isc_I_Ic_n = fisc_I_Ic_n_rhom_Am_BSCCO2212_new(I,Ic,n,rhom,Am)
% Fraction of current in superconductor in a BSCCO2112 strand
% Calculated by voltage balance between superconductor and matrix
% Function by D.S. Davis
% (C) Daniel Davis
% NHMFL, Tallahassee, FL
% Modified from function by E. Ravaioli

if size(I,1)==size(Ic,1)&size(I,2)==size(Ic,2)&size(I,2)==size(n,2)&size(I,1)==size(n,1)
    f_isc_I_Ic_n=zeros(size(I));
elseif size(I,1)==size(Ic,2)&size(I,2)==size(Ic,1)&size(I,2)==size(n,2)&size(I,1)==size(n,1)
    f_isc_I_Ic_n=zeros(size(I));
    Ic=Ic';
elseif size(I,1)==size(n,2)&size(I,2)==size(n,1)&size(I,2)==size(Ic,2)&size(I,1)==size(Ic,1)
    f_isc_I_Ic_n=zeros(size(I));
    n=n';
elseif size(I,1)==size(n,2)&size(I,2)==size(n,1)&size(I,1)==size(Ic,2)&size(I,2)==size(Ic,1)
    f_isc_I_Ic_n=zeros(size(I));
    n=n';
    Ic=Ic';
else
    if size(n)==[1,1]&size(Ic)==[1,1]
        n=ones(size(I))*n;
        Ic=ones(size(I))*Ic;
        f_isc_I_Ic_n=zeros(size(I));
    elseif size(I)==[1,1]&size(Ic)==[1,1]
        I=ones(size(n))*I;
        Ic=ones(size(n))*Ic;
        f_isc_I_Ic_n=zeros(size(n));
    elseif size(n)==[1,1]&size(I)==[1,1]
        n=ones(size(Ic))*n;
        I=ones(size(Ic))*I;
        f_isc_I_Ic_n=zeros(size(Ic)); 
    elseif size(Ic)==[1,1]&size(I,1)==size(n,1)&size(I,2)==size(n,2)
        Ic=ones(size(I))*Ic;
        f_isc_I_Ic_n=zeros(size(I));
    elseif size(Ic)==[1,1]&size(I,1)==size(n,2)&size(I,1)==size(n,2)
        Ic=ones(size(I))*Ic;
        f_isc_I_Ic_n=zeros(size(I));
        n=n';
    elseif size(I)==[1,1]&size(Ic,1)==size(n,1)&size(Ic,2)==size(n,2)
        I=ones(size(Ic))*I;
        f_isc_I_Ic_n=zeros(size(I));
    elseif size(I)==[1,1]&size(Ic,1)==size(n,2)&size(Ic,1)==size(n,2)
        I=ones(size(Ic))*I;
        f_isc_I_Ic_n=zeros(size(Ic));
        n=n';
    elseif size(n)==[1,1]&size(I,1)==size(Ic,1)&size(I,2)==size(Ic,2)
        n=ones(size(I))*n;
        f_isc_I_Ic_n=zeros(size(I));
    elseif size(n)==[1,1]&size(I,1)==size(Ic,2)&size(I,1)==size(Ic,2) 
        n=ones(size(I))*n;
        f_isc_I_Ic_n=zeros(size(I));
        Ic=Ic';
    else
        error('I,Ic and n must be scalars or vectors with the same number of elements.')
    end
end

% % % Modify the inputs
I(I<0)=-I(I<0); % direction of the current is unimportant
Ic(Ic<0)=0; % Critical current must be positive

Ec=0.0001; % [V/m]

% if Ic==0
%   f_isc_I_Ic_n=zeros(size(f_isc_I_Ic_n));
% else
%   x0=ones(size(f_isc_I_Ic_n));
%   func=@(x) I.*x+((I.*x./Ic).^n .*(Ec*Am/rhom))-I;
%   f_isc_I_Ic_n=fsolve(func,x0);
% end

%startGuess=1;
%for t=1:length(f_isc_I_Ic_n)
%    if Ic(t)<=0
%        f_isc_I_Ic_n(t)=0;
%    else
%        func=@(x) I(t).*x+((I(t).*x./Ic(t)).^n(t) .*(Ec*Am./rhom(t)))-I(t);
%        f_isc_I_Ic_n(t)=fzero(func,startGuess);
%    end
%end


for t=1:length(f_isc_I_Ic_n)
  if I(t)==0
    f_isc_I_Ic_n(t)=1;
  elseif Ic(t)<=0
    f_isc_I_Ic_n(t)=0;
  else
     startGuess=1;
     func=@(x) I(t).*x+((I(t).*x./Ic(t)).^n(t) .*(Ec*Am./rhom(t)))-I(t);
     f_isc_I_Ic_n(t)=fzero(func,startGuess);
  end
end

end